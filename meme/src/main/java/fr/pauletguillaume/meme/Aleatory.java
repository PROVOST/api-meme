package fr.pauletguillaume.meme;

import org.springframework.core.io.ClassPathResource;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class Aleatory {
    private List<String> top = new ArrayList<>();
    private List<String> bottom = new ArrayList<>();

    public Aleatory() throws IOException {
        readFiles();
    }

    public void readFiles() throws IOException {
        top.addAll(readFile("/fichier_txt/Top.txt"));
        bottom.addAll(readFile("/fichier_txt/Bottom.txt"));
    }

    private List<String> readFile(String path) throws IOException {
        ArrayList<String> list = new ArrayList<>();
        InputStream fichier = new ClassPathResource(path).getInputStream();
        Scanner scanner = new Scanner(fichier);
        while (scanner.hasNextLine()) {
            list.add(scanner.nextLine());
        }
        return list ;
    }

    public String random(List<String> list){
        Random r = new Random();
        String result = list.get(r.nextInt(list.size()));
        return result;
    }

    public List<String> getBottom() {
        return bottom;
    }

    public List<String> getTop() {
        return top;
    }
}


