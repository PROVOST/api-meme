package fr.pauletguillaume.meme;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;

import okhttp3.*;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.pauletguillaume.meme.storage.MemeDAO;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import okhttp3.Request.Builder;

@RestController

public class MemeController {
	private OkHttpClient client = new OkHttpClient();
	private Map<String, Meme> cacheMeme = new HashMap<>();
	private List<String> imageList = new ArrayList<>();
	private ObjectMapper objectMapper = new ObjectMapper();
	
	@Autowired
	private MemeDAO memeDAO;
	
	public MemeController() {
		objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
	}
	
	@GetMapping(value = "/mesmemes")
	public String index() {
		return "Hello World !";
	}
	
	@GetMapping(value = "/meme/{meme}")
	public Meme getInfos(@PathVariable String meme) throws IOException {
		if (cacheMeme.get(meme) != null) {
			return cacheMeme.get(meme);
		}
		
		Request request = new Request.Builder()
				.url("").build();
		Response response = client.newCall(request).execute();
		
		Meme mappedMeme = objectMapper.readValue(response.body().string(), Meme.class);
		cacheMeme.put(meme, mappedMeme);
		
		return mappedMeme;
	}
	
	/*@GetMapping(value = "/image/{name}")
	public String getNomImage(@PathVariable String name) throws IOException {
		if (imageList.contains(name)) {
			return imageList.toString();
		}
		
		Request request = new Request.Builder()
				.url("http://apimeme.com/thumbnail?name=" + name).build();
		Response response = client.newCall(request).execute();
		
		//Image mappedImage = objectMapper.readValue(response.body().string(), Image.class);
		System.out.println(response.body());
		String mappedImage = response.body().string();
		System.out.println(mappedImage);
		
		if(mappedImage != null) {
			imageList.add(name);
		}
		//InputStream mappedImage = response.body().byteStream();	
		//BufferedImage mappedImageBuffered = ImageIO.read(mappedImage);
		//System.out.println(mappedImageBuffered);
		//String mappedImageName = mappedImage.toString();
		//System.out.println(mappedImageName);	
		//imageList.add(mappedImageName);
		
		
		BufferedReader in = new BufferedReader(new FileReader("http://apimeme.com/index"));
		String line;
		while ((line = in.readLine())!= null) {
			if (line.contains("option")) {
				int debut = line.indexOf("\"");
				int fin = line.indexOf("\"", debut);
				name = line.substring(debut + 1, fin);
				imageList.add(name);
			}
			System.out.println(line);
		}
		return imageList.toString();
	}*/
	
	@GetMapping(value = "/image/name")
	public String getNomImage2() throws IOException {
		String name = null;
		if (imageList.contains(name)) {
			return imageList.toString();
		}
		
		/*Request request = new Request.Builder()
				.url("http://apimeme.com").build();
		Response response = client.newCall(request).execute();*/
		
		Document doc = Jsoup.connect("http://apimeme.com").get();
		Elements doc2 = doc.select("option");
		for(int i = 0; i<doc2.size(); i++) {
			name = doc2.get(i).attr("value");
			imageList.add(name);
		}
	
		return imageList.toString();
	}

	@GetMapping(value = "/meme")
		public String randomMeme()throws IOException{
			Aleatory aleatory = new Aleatory();
			Request request = new Request.Builder()
					.url("http://apimeme.com/meme?meme=10-Guy&top="+aleatory.random(aleatory.getTop())+"&bottom="+aleatory.random(aleatory.getBottom())).build();
		Response response = client.newCall(request).execute();
		return ("http://apimeme.com/meme?meme=10-Guy&top="+aleatory.random(aleatory.getTop())+"&bottom="+aleatory.random(aleatory.getBottom()));
	}





	
	public MemeDAO getMemeDAO() {
		return memeDAO;
	}
	
	public void setMemeDAO(MemeDAO memeDAO) {
		this.memeDAO = memeDAO;
	}
	
}