package fr.pauletguillaume.meme;

public class Meme{
	private int id = 1;
	private Image image;
	private String bottomText;
	private String topText;
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	 public Image getImage() {
		 return image;
	 }

	 public void setImage(Image image) {
		 this.image = image;
	 }
	    
	 public String getBottomText() {
	     return bottomText;
	 }

	 public void setBottomText(String bottomText) {
		 this.bottomText = bottomText;
	 }
	    
	 public String getTopText() {
	     return topText;
	 }

	 public void setTopText(String topText) {
	     this.topText = topText;
	 }
	
	
}
