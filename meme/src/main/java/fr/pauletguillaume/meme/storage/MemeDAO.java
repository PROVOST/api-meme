package fr.pauletguillaume.meme.storage;

import java.util.List;

import fr.pauletguillaume.meme.Meme;

public interface MemeDAO {

    public List<Meme> getMyMemes();
}
