package fr.pauletguillaume.meme.storage;


import fr.pauletguillaume.meme.Meme;
import fr.pauletguillaume.meme.storage.MemeDAO;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import fr.pauletguillaume.meme.Meme;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

@Service
@ConditionalOnProperty(name = "storagemode", havingValue = "file", matchIfMissing = true)
public class MemeDAOFichier implements MemeDAO {

    public List<Meme> getMyMemes() {
        System.out.println("Using fichier");
        File file = new File("memes.json");
        //Files.readAllLines(file.toPath());
        return new ArrayList<>();
    }
}
